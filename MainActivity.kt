package com.mts.talker

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_start.*
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        Constants.isIntiatedNow = true
        Constants.isCallEnded = true
        register.setOnClickListener {

            val okHttpClient = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(39, TimeUnit.SECONDS)
                .hostnameVerifier { _, _ -> true }
                .build()

            val serverUrl = "wss://coturn.axat.one"

            val request = Request.Builder().url(serverUrl).build()

            Log.d("WEBRTC", "wss: create connection, wss host: $serverUrl")

            WebServiceProvider.webRTCWebSocketListener = WebRTCWebSocketListener()
            WebServiceProvider.webRTCWebSocketListener!!.initPeerConnectionFactory(application)
            WebServiceProvider.webRTCWebSocketListener!!.setVideoCapturer(this.applicationContext)
            WebServiceProvider.webSocket = okHttpClient.newWebSocket(request, WebServiceProvider.webRTCWebSocketListener)
        }
        start_call.setOnClickListener {
            if (meeting_id.text.toString().trim().isNullOrEmpty())
                meeting_id.error = "Please enter callee"
            else {
                val intent = Intent(this@MainActivity, RTCActivity::class.java)
                intent.putExtra("id", meeting_id.text.toString())
                WebServiceProvider.callee = meeting_id.text.toString()
                startActivity(intent)
            }
        }
    }
}