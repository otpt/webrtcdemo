package com.mts.talker

import android.app.Application
import android.content.Context
import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.webrtc.*


class RTCClient(
        context: Application,
        observer: PeerConnection.Observer
) {

    private val peerConnection by lazy { buildPeerConnection(observer) }

    companion object {
        private const val LOCAL_TRACK_ID = "local_track"
        private const val LOCAL_STREAM_ID = "local_track"
    }

    private val rootEglBase: EglBase = EglBase.create()

    private var localAudioTrack : AudioTrack? = null
    private var localVideoTrack : VideoTrack? = null
    val TAG = "RTCClient"

    var remoteSessionDescription : SessionDescription? = null

    val db = Firebase.firestore

    init {
        initPeerConnectionFactory(context)
    }

    private val iceServer = listOf(
            PeerConnection.IceServer.builder("turn:coturn.axat.one:8443?transport=tcp")
                    .setHostname("streamuser")
                    .setPassword("StreamPassword")
                    .createIceServer()
    )

    private val peerConnectionFactory by lazy { buildPeerConnectionFactory() }

    private val audioSource by lazy { peerConnectionFactory.createAudioSource(MediaConstraints())}

    private fun initPeerConnectionFactory(context: Application) {
        val options = PeerConnectionFactory.InitializationOptions.builder(context)
                .setEnableInternalTracer(true)
                .setFieldTrials("WebRTC-H264HighProfile/Enabled/")
                .createInitializationOptions()
        PeerConnectionFactory.initialize(options)
    }

    private fun buildPeerConnectionFactory(): PeerConnectionFactory {
        return PeerConnectionFactory
                .builder()
                .setOptions(PeerConnectionFactory.Options().apply {
                    // disableEncryption = true
                    // disableNetworkMonitor = true
                })
                .createPeerConnectionFactory()
    }

    private fun buildPeerConnection(observer: PeerConnection.Observer) = peerConnectionFactory.createPeerConnection(
            iceServer,
            observer
    )

//    private fun PeerConnection.call(sdpObserver: SdpObserver, meetingID: String) {
//        val constraints = MediaConstraints()
//
//        createOffer(object : SdpObserver by sdpObserver {
//            override fun onCreateSuccess(desc: SessionDescription?) {
//                setLocalDescription(object : SdpObserver {
//                    override fun onSetFailure(p0: String?) {
//                        Log.e(TAG, "onSetFailure: $p0")
//                    }
//
//                    override fun onSetSuccess() {
//                        val offer = hashMapOf(
//                                "sdp" to desc?.description,
//                                "type" to desc?.type
//                        )
//
//                        WebServiceProvider.webSocket!!.send("{\"type\" : \"offer\", \"data\": {\"to\" : \"${WebServiceProvider.callee}\", \"from\" : \"valery\", \"sdp\" : ${offer.toString()}}}")
////                        db.collection("calls").document(meetingID)
////                                .set(offer)
////                                .addOnSuccessListener {
////                                    Log.e(TAG, "DocumentSnapshot added")
////                                }
////                                .addOnFailureListener { e ->
////                                    Log.e(TAG, "Error adding document", e)
////                                }
////                        Log.e(TAG, "onSetSuccess")
//                    }
//
//                    override fun onCreateSuccess(p0: SessionDescription?) {
//                        Log.e(TAG, "onCreateSuccess: Description $p0")
//                    }
//
//                    override fun onCreateFailure(p0: String?) {
//                        Log.e(TAG, "onCreateFailure: $p0")
//                    }
//                }, desc)
//                sdpObserver.onCreateSuccess(desc)
//            }
//
//            override fun onSetFailure(p0: String?) {
//                Log.e(TAG, "onSetFailure: $p0")
//            }
//
//            override fun onCreateFailure(p0: String?) {
//                Log.e(TAG, "onCreateFailure: $p0")
//            }
//        }, constraints)
//    }

    private fun PeerConnection.answer(sdpObserver: SdpObserver, meetingID: String) {
        val constraints = MediaConstraints().apply {
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"))
        }
        createAnswer(object : SdpObserver by sdpObserver {
            override fun onCreateSuccess(desc: SessionDescription?) {
                val answer = hashMapOf(
                        "sdp" to desc?.description,
                        "type" to desc?.type
                )
                db.collection("calls").document(meetingID)
                        .set(answer)
                        .addOnSuccessListener {
                            Log.e(TAG, "DocumentSnapshot added")
                        }
                        .addOnFailureListener { e ->
                            Log.e(TAG, "Error adding document", e)
                        }
                setLocalDescription(object : SdpObserver {
                    override fun onSetFailure(p0: String?) {
                        Log.e(TAG, "onSetFailure: $p0")
                    }

                    override fun onSetSuccess() {
                        Log.e(TAG, "onSetSuccess")
                    }

                    override fun onCreateSuccess(p0: SessionDescription?) {
                        Log.e(TAG, "onCreateSuccess: Description $p0")
                    }

                    override fun onCreateFailure(p0: String?) {
                        Log.e(TAG, "onCreateFailureLocal: $p0")
                    }
                }, desc)
                sdpObserver.onCreateSuccess(desc)
            }

            override fun onCreateFailure(p0: String?) {
                Log.e(TAG, "onCreateFailureRemote: $p0")
            }
        }, constraints)
    }

    // fun call(sdpObserver: SdpObserver, meetingID: String) = peerConnection?.call(sdpObserver, meetingID)

    fun answer(sdpObserver: SdpObserver, meetingID: String) = peerConnection?.answer(sdpObserver, meetingID)

    fun onRemoteSessionReceived(sessionDescription: SessionDescription) {
        remoteSessionDescription = sessionDescription
        peerConnection?.setRemoteDescription(object : SdpObserver {
            override fun onSetFailure(p0: String?) {
                Log.e(TAG, "onSetFailure: $p0")
            }

            override fun onSetSuccess() {
                Log.e(TAG, "onSetSuccessRemoteSession")
            }

            override fun onCreateSuccess(p0: SessionDescription?) {
                Log.e(TAG, "onCreateSuccessRemoteSession: Description $p0")
            }

            override fun onCreateFailure(p0: String?) {
                Log.e(TAG, "onCreateFailure")
            }
        }, sessionDescription)

    }

    fun addIceCandidate(iceCandidate: IceCandidate?) {

    }

    fun endCall(meetingID: String) {
//        db.collection("calls").document(meetingID).collection("candidates")
//                .get().addOnSuccessListener {
//                    val iceCandidateArray: MutableList<IceCandidate> = mutableListOf()
//                    for ( dataSnapshot in it) {
//                        if (dataSnapshot.contains("type") && dataSnapshot["type"]=="offerCandidate") {
//                            val offerCandidate = dataSnapshot
//                            iceCandidateArray.add(IceCandidate(offerCandidate["sdpMid"].toString(), Math.toIntExact(offerCandidate["sdpMLineIndex"] as Long), offerCandidate["sdp"].toString()))
//                        } else if (dataSnapshot.contains("type") && dataSnapshot["type"]=="answerCandidate") {
//                            val answerCandidate = dataSnapshot
//                            iceCandidateArray.add(IceCandidate(answerCandidate["sdpMid"].toString(), Math.toIntExact(answerCandidate["sdpMLineIndex"] as Long), answerCandidate["sdp"].toString()))
//                        }
//                    }
//                    peerConnection?.removeIceCandidates(iceCandidateArray.toTypedArray())
//                }
//        val endCall = hashMapOf(
//                "type" to "END_CALL"
//        )
//        db.collection("calls").document(meetingID)
//                .set(endCall)
//                .addOnSuccessListener {
//                    Log.e(TAG, "DocumentSnapshot added")
//                }
//                .addOnFailureListener { e ->
//                    Log.e(TAG, "Error adding document", e)
//                }

        WebServiceProvider.webRTCWebSocketListener!!.onClosing(WebServiceProvider.webSocket!!, -1, null)

        peerConnection?.close()
    }


    fun enableAudio(audioEnabled: Boolean) {
        if (localAudioTrack != null)
            localAudioTrack?.setEnabled(audioEnabled)
    }
}