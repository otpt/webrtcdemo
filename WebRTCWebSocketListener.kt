package com.mts.talker

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.core.view.isGone
import com.mts.talker.WebServiceProvider.callee
import com.mts.talker.WebServiceProvider.webSocket
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import org.json.JSONObject
import org.webrtc.*

class WebRTCWebSocketListener : WebSocketListener()  {

    val NORMAL_CLOSURE_STATUS = 1000;

    private val rootEglBase: EglBase = EglBase.create()

    private val peerConnection by lazy { buildPeerConnection(observer) }

    companion object {
        private const val LOCAL_TRACK_ID = "local_track"
        private const val LOCAL_STREAM_ID = "local_track"
    }

    private var localAudioTrack : AudioTrack? = null
    private var localVideoTrack : VideoTrack? = null
    private var videoCapturer: CameraVideoCapturer? = null // by lazy { getVideoCapturer(context) }

    private val iceServer = listOf(
            PeerConnection.IceServer.builder("turn:coturn.axat.one:8443?transport=tcp")
                    .setHostname("streamuser")
                    .setPassword("StreamPassword")
                    .createIceServer()
    )

    private val peerConnectionFactory by lazy { buildPeerConnectionFactory() }

    private val audioSource by lazy { peerConnectionFactory.createAudioSource(MediaConstraints())}
    private val localVideoSource by lazy { peerConnectionFactory.createVideoSource(false) }

    fun setVideoCapturer(context: Context) {
        videoCapturer = getVideoCapturer(context)
    }

    fun checkPeerConnection() {
        if (peerConnection === null) {
            Log.d("WEBRTC", "PEER CONNECTION IS NULL")
        } else {
            Log.d("WEBRTC", "PEER CONNECTION IS NOT NULL")
        }
    }

    private fun getVideoCapturer(context: Context) =
            Camera2Enumerator(context).run {
                deviceNames.find {
                    isFrontFacing(it)
                }?.let {
                    createCapturer(it, null)
                } ?: throw IllegalStateException()
            }

    fun initSurfaceView(view: SurfaceViewRenderer) = view.run {
        setMirror(true)
        setEnableHardwareScaler(true)
        init(rootEglBase.eglBaseContext, null)
    }

    fun startLocalVideoCapture(localVideoOutput: SurfaceViewRenderer) {
        localAudioTrack = peerConnectionFactory.createAudioTrack(LOCAL_TRACK_ID + "_audio", audioSource);
        val localStream = peerConnectionFactory.createLocalMediaStream(LOCAL_STREAM_ID)
        localStream.addTrack(localAudioTrack)
        peerConnection?.addStream(localStream)

//        val surfaceTextureHelper = SurfaceTextureHelper.create(Thread.currentThread().name, rootEglBase.eglBaseContext)
//        (videoCapturer as VideoCapturer).initialize(surfaceTextureHelper, localVideoOutput.context, localVideoSource.capturerObserver)
//        videoCapturer!!.startCapture(320, 240, 60)
//        localAudioTrack = peerConnectionFactory.createAudioTrack(LOCAL_TRACK_ID + "_audio", audioSource);
//        localVideoTrack = peerConnectionFactory.createVideoTrack(LOCAL_TRACK_ID, localVideoSource)
//        localVideoTrack?.addSink(localVideoOutput)
//        val localStream = peerConnectionFactory.createLocalMediaStream(LOCAL_STREAM_ID)
//        localStream.addTrack(localVideoTrack)
//        localStream.addTrack(localAudioTrack)
//        peerConnection?.addStream(localStream)
    }

    private val observer : PeerConnection.Observer = object : PeerConnectionObserver() {
        override fun onIceCandidate(candidate: IceCandidate?) {
            super.onIceCandidate(candidate)
            webSocket!!.send("{\"type\" : \"candidate\", \"data\" : {\"to\" : \"${WebServiceProvider.callee}\", \"from\" : \"valery\", \"candidate\" : \"${candidate.toString()}\"}}")
            peerConnection?.addIceCandidate(candidate)
        }

        override fun onAddStream(p0: MediaStream?) {
            super.onAddStream(p0)
            Log.e("WEBRTC", "onAddStream: $p0")
        }

        override fun onIceConnectionChange(p0: PeerConnection.IceConnectionState?) {
            Log.e("WEBRTC", "onIceConnectionChange: $p0")
        }

        override fun onIceConnectionReceivingChange(p0: Boolean) {
            Log.e("WEBRTC", "onIceConnectionReceivingChange: $p0")
        }

        override fun onConnectionChange(newState: PeerConnection.PeerConnectionState?) {
            Log.e("WEBRTC", "onConnectionChange: $newState")
        }

        override fun onDataChannel(p0: DataChannel?) {
            Log.e("WEBRTC", "onDataChannel: $p0")
        }

        override fun onStandardizedIceConnectionChange(newState: PeerConnection.IceConnectionState?) {
            Log.e("WEBRTC", "onStandardizedIceConnectionChange: $newState")
        }

        override fun onAddTrack(p0: RtpReceiver?, p1: Array<out MediaStream>?) {
            Log.e("WEBRTC", "onAddTrack: $p0 \n $p1")
        }

        override fun onTrack(transceiver: RtpTransceiver?) {
            Log.e("WEBRTC", "onTrack: $transceiver" )
        }
    }

    private val sdpObserver = object : AppSdpObserver() {
        override fun onCreateSuccess(p0: SessionDescription?) {
            super.onCreateSuccess(p0)
        }
    }

    fun initPeerConnectionFactory(context: Application) {
        val options = PeerConnectionFactory.InitializationOptions.builder(context)
                .setEnableInternalTracer(true)
                .setFieldTrials("WebRTC-H264HighProfile/Enabled/")
                .createInitializationOptions()
        PeerConnectionFactory.initialize(options)
    }

    private fun buildPeerConnectionFactory(): PeerConnectionFactory {
        return PeerConnectionFactory
                .builder()
                .setOptions(PeerConnectionFactory.Options().apply {
                    // disableEncryption = true
                    // disableNetworkMonitor = true
                })
                .createPeerConnectionFactory()
    }

    private fun buildPeerConnection(observer: PeerConnection.Observer) = peerConnectionFactory.createPeerConnection(
            iceServer,
            observer
    )

    override fun onOpen(webSocket: WebSocket, response: Response?) {
        webSocket.send("{\"type\" : \"register\", \"data\": {\"from\" : \"valery\"}}")
    }

    private fun PeerConnection.call(sdpObserver: SdpObserver, meetingID: String) {
        Log.d("WEBRTC", "CALL STARTED")

        val constraints = MediaConstraints().apply {
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "false"))
        }

        createOffer(object : SdpObserver by sdpObserver {
            override fun onCreateSuccess(desc: SessionDescription?) {
                setLocalDescription(object : SdpObserver {
                    override fun onSetFailure(p0: String?) {
                        Log.e("WEBRTC", "onSetFailure: $p0")
                    }

                    override fun onSetSuccess() {
//                        val offer = hashMapOf(
//                                "sdp" to desc?.description,
//                                "type" to desc?.type
//                        )
//                        db.collection("calls").document(meetingID)
//                                .set(offer)
//                                .addOnSuccessListener {
//                                    Log.e(TAG, "DocumentSnapshot added")
//                                }
//                                .addOnFailureListener { e ->
//                                    Log.e(TAG, "Error adding document", e)
//                                }
//                        Log.e(TAG, "onSetSuccess")
                    }

                    override fun onCreateSuccess(p0: SessionDescription?) {
                        Log.e("WEBRTC", "onCreateSuccess: Description $p0")
                    }

                    override fun onCreateFailure(p0: String?) {
                        Log.e("WEBRTC", "onCreateFailure: $p0")
                    }
                }, desc)
                sdpObserver.onCreateSuccess(desc)
            }

            override fun onSetFailure(p0: String?) {
                Log.e("WEBRTC", "onSetFailure: $p0")
            }

            override fun onCreateFailure(p0: String?) {
                Log.e("WEBRTC", "onCreateFailure: $p0")
            }
        }, constraints)
    }

    fun call(sdpObserver: SdpObserver, meetingID: String) = peerConnection?.call(sdpObserver, meetingID)


    private fun PeerConnection.answer(sdpObserver: SdpObserver) {
        val constraints = MediaConstraints().apply {
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "false"))
        }
        createAnswer(object : SdpObserver by sdpObserver {
            override fun onCreateSuccess(desc: SessionDescription?) {
                Log.d("WEBRTC", "---> Sending the answer to the remote peer")
                webSocket!!.send("\"name\" : \"valery\", \"target\" : \"$callee\", \"type\" : \"answer\", \"data\" : {\"to\" : \"$callee\", \"from\" : \"valery\", \"sdp\" : ${peerConnection!!.localDescription.description}")

                setLocalDescription(object : SdpObserver {
                    override fun onSetFailure(p0: String?) {
                        Log.e("WEBRTC", "onSetFailure: $p0")
                    }

                    override fun onSetSuccess() {
                        Log.e("WEBRTC", "onSetSuccess")
                    }

                    override fun onCreateSuccess(p0: SessionDescription?) {
                        Log.e("WEBRTC", "onCreateSuccess: Description $p0")
                    }

                    override fun onCreateFailure(p0: String?) {
                        Log.e("WEBRTC", "onCreateFailureLocal: $p0")
                    }
                }, desc)
                sdpObserver.onCreateSuccess(desc)
            }

            override fun onCreateFailure(p0: String?) {
                Log.e("WEBRTC", "onCreateFailureRemote: $p0")
            }
        }, constraints)
    }

    override fun onMessage(webSocket: WebSocket, text: String?) {
        Log.d("WEBRTC", "Receiving : $text")

        val jObj = JSONObject(text)

        when (jObj.getString("type")) {
            "offer" -> {
                callee = JSONObject(text).getString("from")

                Log.d("WEBRTC", "wss:handleoffermsg received offer from user: $callee")

                var remoteDescription = SessionDescription(SessionDescription.Type.OFFER, jObj.getJSONObject("data").getString("sdp"))
                peerConnection?.setRemoteDescription(object : SdpObserver {
                    override fun onSetFailure(p0: String?) {
                        Log.e("WEBRTC", "onSetFailure: $p0")
                    }
                    override fun onSetSuccess() {
                        Log.e("WEBRTC", "onSetSuccessRemoteSession")
                    }

                    override fun onCreateSuccess(p0: SessionDescription?) {
                        Log.e("WEBRTC", "onCreateSuccessRemoteSession: Description $p0")
                    }

                    override fun onCreateFailure(p0: String?) {
                        Log.e("WEBRTC", "onCreateFailure")
                    }
                }, remoteDescription)
                Constants.isIntiatedNow = false

                peerConnection?.answer(sdpObserver)
            }
            "answer" -> {
                Log.d("WEBRTC", "wss:handleAnswerMsg call recipient has accepted our call");

                val remoteSessionDescription = SessionDescription(SessionDescription.Type.OFFER, jObj.getJSONObject("data").getString("sdp"))
                peerConnection?.setRemoteDescription(object : SdpObserver {
                    override fun onSetFailure(p0: String?) {
                        Log.e("WEBRTC", "onSetFailure: $p0")
                    }

                    override fun onSetSuccess() {
                        Log.e("WEBRTC", "onSetSuccessRemoteSession")
                    }

                    override fun onCreateSuccess(p0: SessionDescription?) {
                        Log.e("WEBRTC", "onCreateSuccessRemoteSession: Description $p0")
                    }

                    override fun onCreateFailure(p0: String?) {
                        Log.e("WEBRTC", "onCreateFailure")
                    }
                }, remoteSessionDescription)
                Constants.isIntiatedNow = false
            }
            "candidate" -> {}// handleNewICECandidateMsg(msg)
            "bye" -> { webSocket.send("{\"type\" : \"bye\", \"data\": {\"to\" : \"$callee\", \"from\" : \"valery\"}}") }
            "turnConfig" -> {}// configurationFromServer = msg.data
            else -> Log.d("WEBRTC" , "wss:incoming: unknown message type")
        }
    }

    override fun  onClosing(webSocket: WebSocket, code: Int, reason: String?) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        Log.d("WEBRTC", "Closing : $code / $reason");
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        Log.d("WEBRTC", "Error : " + t.message);
    }

    fun enableAudio(audioEnabled: Boolean) {
        if (localAudioTrack != null)
            localAudioTrack?.setEnabled(audioEnabled)
    }

    fun endCall() {
//        db.collection("calls").document(meetingID).collection("candidates")
//                .get().addOnSuccessListener {
//                    val iceCandidateArray: MutableList<IceCandidate> = mutableListOf()
//                    for ( dataSnapshot in it) {
//                        if (dataSnapshot.contains("type") && dataSnapshot["type"]=="offerCandidate") {
//                            val offerCandidate = dataSnapshot
//                            iceCandidateArray.add(IceCandidate(offerCandidate["sdpMid"].toString(), Math.toIntExact(offerCandidate["sdpMLineIndex"] as Long), offerCandidate["sdp"].toString()))
//                        } else if (dataSnapshot.contains("type") && dataSnapshot["type"]=="answerCandidate") {
//                            val answerCandidate = dataSnapshot
//                            iceCandidateArray.add(IceCandidate(answerCandidate["sdpMid"].toString(), Math.toIntExact(answerCandidate["sdpMLineIndex"] as Long), answerCandidate["sdp"].toString()))
//                        }
//                    }
//                    peerConnection?.removeIceCandidates(iceCandidateArray.toTypedArray())
//                }
//        val endCall = hashMapOf(
//                "type" to "END_CALL"
//        )
//        db.collection("calls").document(meetingID)
//                .set(endCall)
//                .addOnSuccessListener {
//                    Log.e(TAG, "DocumentSnapshot added")
//                }
//                .addOnFailureListener { e ->
//                    Log.e(TAG, "Error adding document", e)
//                }

        WebServiceProvider.webRTCWebSocketListener!!.onClosing(WebServiceProvider.webSocket!!, -1, null)
        peerConnection?.close()
    }
}
